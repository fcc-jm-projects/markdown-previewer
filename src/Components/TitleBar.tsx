import React from 'react'

import { ReactComponent as CompressIcon } from '../icons/compress-arrows-alt-solid.svg'
import { ReactComponent as ExpandIcon } from '../icons/expand-arrows-alt-solid.svg'
import { ReactComponent as FCCIcon } from '../icons/free-code-camp-brands.svg'

interface TitleBar {
   title: string
   isExpanded: boolean
   handleOnClick: () => void
}

const TitleBar: React.FC<TitleBar> = ({ title, isExpanded, handleOnClick }) => {
   // we need to use the opposite icon of it's current condition
   const Icon = !isExpanded ? ExpandIcon : CompressIcon

   const expandOrCompress = isExpanded ? 'expand' : 'compress'

   return (
      <div className="title__wrapper">
         <div>
            <FCCIcon className="icon__fcc" />
            <span className="title">{title}</span>
         </div>
         <Icon className={`icon__${expandOrCompress}`} onClick={handleOnClick} />
      </div>
   )
}

export { TitleBar }
