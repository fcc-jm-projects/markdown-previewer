import React, { ChangeEvent, Fragment, useEffect, useRef } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import {
   getEditorHeight,
   getEditorText,
   getExpandedElement,
   getReformattedText,
   toggleEditorSize,
   togglePreviewSize,
   updateEditorHeight,
   updateEditorText
} from '../redux/EditorReducer'
import { TitleBar } from './TitleBar'

const App: React.FC = () => {
   const dispatch = useDispatch()

   const textArea = useRef<HTMLTextAreaElement>(null)

   const textAreaHeight = useSelector(getEditorHeight)
   const expandedElement = useSelector(getExpandedElement)
   const editorText = useSelector(getEditorText)
   const reformattedText = useSelector(getReformattedText)

   const editorIsExpanded = expandedElement === 'editor'
   const previewIsExpanded = expandedElement === 'preview'
   // we need to use the opposite icon of it's current state
   const expandOrCompress = expandedElement ? 'expand' : 'compress'

   const toggleEditor = () => {
      if (!expandedElement && textArea && textArea.current && textArea.current.style.height) {
         dispatch(updateEditorHeight(textArea.current.style.height))
      }

      dispatch(toggleEditorSize())
   }

   const togglePreview = () => {
      dispatch(togglePreviewSize())
   }

   const handleTextChange = (e: ChangeEvent<HTMLTextAreaElement>) =>
      dispatch(updateEditorText(e.target.value))

   useEffect(() => {
      if (textArea && textArea.current) textArea.current.focus()
   }, [])

   return (
      <Fragment>
         {(expandedElement === 'editor' || expandedElement === '') && (
            <section className={`editor editor--${expandOrCompress}`}>
               <TitleBar title="Editor" isExpanded={editorIsExpanded} handleOnClick={toggleEditor} />
               <textarea
                  className={`editor__text-area editor__text-area--${expandOrCompress}`}
                  id="editor"
                  value={editorText}
                  onChange={handleTextChange}
                  ref={textArea}
                  style={{ height: editorIsExpanded ? '92vh' : textAreaHeight }}
               />
            </section>
         )}
         {(expandedElement === '' || expandedElement === 'preview') && (
            <section className="preview">
               <TitleBar title="Preview" isExpanded={previewIsExpanded} handleOnClick={togglePreview} />
               <div
                  dangerouslySetInnerHTML={{ __html: reformattedText }}
                  id="preview"
                  className="preview__markdown"
               />
            </section>
         )}
      </Fragment>
   )
}

export { App }
