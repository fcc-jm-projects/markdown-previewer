import 'modern-normalize/modern-normalize.css'
import './styles/index.scss'
import './styles/App.scss'
import 'highlight.js/styles/github.css'

import React, { Fragment } from 'react'
import ReactDOM from 'react-dom'
import { Helmet } from 'react-helmet'
import { Provider } from 'react-redux'

import { App } from './Components/App'
import { store } from './redux/store'
import * as serviceWorker from './serviceWorker'

const Root = () => {
   return (
      <Fragment>
         <Helmet>
            <title>Markdown Previewer</title>
            <script src="https://cdn.freecodecamp.org/testable-projects-fcc/v1/bundle.js"></script>
         </Helmet>
         <Provider store={store}>
            <App />
         </Provider>
      </Fragment>
   )
}

ReactDOM.render(<Root />, document.getElementById('root'))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
