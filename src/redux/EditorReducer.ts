import hljs from 'highlight.js'
import marked from 'marked'

import { defaultEditorText } from './defaultEditorText'
import { Store } from './store'

const initialState = {
   expandedElement: '',
   text: defaultEditorText,
   height: '192px'
}

marked.setOptions({
   highlight: function(code) {
      return hljs.highlightAuto(code).value
   },
   breaks: true
})

/**
 * selectors
 */
const getExpandedElement = (state: Store) => state.editor.expandedElement
const getEditorText = (state: Store) => state.editor.text
const getEditorHeight = (state: Store) => state.editor.height
const getReformattedText = (state: Store) => marked(state.editor.text)

/**
 * actions
 */
const TOGGLE_EDITOR_SIZE = 'TOGGLE_EDITOR_SIZE'
const TOGGLE_PREVIEW_SIZE = 'TOGGLE_PREVIEW_SIZE'
const UPDATE_EDITOR_TEXT = 'UPDATE_EDITOR_STATE'
const UPDATE_EDITOR_HEIGHT = 'UPDATE_EDITOR_HEIGHT'

const toggleEditorSize = () => ({
   type: TOGGLE_EDITOR_SIZE
})

const togglePreviewSize = () => ({
   type: TOGGLE_PREVIEW_SIZE
})

const updateEditorText = (updatedText: string) => ({
   type: UPDATE_EDITOR_TEXT,
   payload: updatedText
})

const updateEditorHeight = (height: string) => ({
   type: UPDATE_EDITOR_HEIGHT,
   payload: height
})

const EditorReducer = (state = initialState, action: any) => {
   switch (action.type) {
      case TOGGLE_EDITOR_SIZE:
         return { ...state, expandedElement: state.expandedElement ? '' : 'editor' }
      case TOGGLE_PREVIEW_SIZE:
         return { ...state, expandedElement: state.expandedElement ? '' : 'preview' }
      case UPDATE_EDITOR_TEXT:
         return { ...state, text: action.payload }
      case UPDATE_EDITOR_HEIGHT:
         return { ...state, height: action.payload }
      default:
         return state
   }
}

export {
   EditorReducer,
   getEditorHeight,
   getExpandedElement,
   getEditorText,
   getReformattedText,
   toggleEditorSize,
   togglePreviewSize,
   updateEditorHeight,
   updateEditorText
}
