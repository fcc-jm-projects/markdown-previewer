import { combineReducers, createStore } from 'redux'

import { EditorReducer } from './EditorReducer'

const isDev = (process.env.NODE_ENV || '') === 'development'

const rootReducer = combineReducers({
   editor: EditorReducer
})

// @ts-ignore
const initalState = isDev ? window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__() : {}

const store = createStore(rootReducer, initalState)

export type Store = ReturnType<typeof rootReducer>
export { store }
